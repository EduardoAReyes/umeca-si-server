import express from "express";
const rutaImputados = express();
import barril from "../controladores.js";
import controlador from "../controlador/Imputados.js";


rutaImputados.get("/consultarImputadosReg", barril.consultarImputadosReg);

rutaImputados.post(
  "/registrarAsistenciaImputado",
  barril.registrarAsistenciaImputado
);

rutaImputados.get(
  "/consultarAsistenciaImputados",
  barril.consultarAsistenciaImputados
);

rutaImputados.get("/consultarImputados", barril.consultarImputado);

rutaImputados.post("/ingresarImputado", barril.ingresarImputado);

rutaImputados.post("/eliminarImputado", barril.eliminarImputado);

rutaImputados.post("/actualizarImputado", barril.actualizarImputado);

rutaImputados.post("/capturarRostroImpu", controlador.capturarRostroImpu);


export default rutaImputados;
