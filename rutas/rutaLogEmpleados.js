import e from "express";
import controlador from "../controlador/logEmpleados.js";

const rutaLogEmpleados = e();

rutaLogEmpleados.post("/registrarPassword", controlador.registrarPassword);
rutaLogEmpleados.post("/login",controlador.login);

export default rutaLogEmpleados;
