/**
 * Aqui se crea una ruta del servidor, la cual recibira una peticion post
 */

import express from "express";
const rutaRostros = express.Router();
import controlador from "../controlador/controladorRostros.js";

rutaRostros.post("/rostros", controlador.prueba);

export default rutaRostros;
