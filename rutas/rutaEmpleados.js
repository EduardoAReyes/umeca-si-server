import express from "express";
import barril from "../controladores.js";
import controlador from "../controlador/empleados.js";
const rutaEmpleados = express();


rutaEmpleados.get("/consultarEmpleadosReg", barril.consultarEmpleadosReg);

rutaEmpleados.get("/consultarEmpleados", barril.consultarEmpleado);

rutaEmpleados.post("/agregarEmpleado", barril.agregarEmpleado);

rutaEmpleados.post("/eliminarEmpleado", barril.eliminarEmpleado);

rutaEmpleados.post("/actualizarEmpleado", barril.actualizarEmpleado);



rutaEmpleados.post("/capturarRostroEmp", controlador.capturarRostroEmp);

rutaEmpleados.post("/eliminarCapturasEmp", controlador.eliminarCapturasEmp);



export default rutaEmpleados;
