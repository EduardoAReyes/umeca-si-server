import express from "express";
import controlador from "../controlador/controladorConexion.js";
const rutaConexion = express();
rutaConexion.get("/conexion", controlador.comprobarConexion);
export default rutaConexion;
