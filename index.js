import express from "express";
import morgan from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
const app = express();
import rutaImputados from "./rutas/rutaImputados.js";
import rutaEmpleados from "./rutas/rutaEmpleados.js";
import rutaLogEmpleados from "./rutas/rutaLogEmpleados.js";

/**
 * Archivo index.js donde se implementa la configuracion del servidor local hecho con nodejs y expressjs.
 */

app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.set("port", process.env.PORT || 3000);
app.use(express.json());
app.use(morgan("dev"));
// app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", rutaImputados);
app.use("/", rutaEmpleados);
app.use("/", rutaLogEmpleados);

app.listen(app.get("port"), () => {
  console.log("Servidor en 3000");
});
