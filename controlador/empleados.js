import { sqlConfig } from "../conexion.js";
import sql from "mssql";
import fs from "fs";
import path from "path";
const controlador = {};

controlador.registrarContraseña = async (req, res) => {
  try {
    const usuario = req.body.usuario;
    const contraseña = req.body.contraseña;

    let query = ``;

    let pool = await sql.connect(sqlConfig);

    let result = await pool.request().query();

  } catch (error) {
    res.status(500).send("hubo un error", error);
  }
};


controlador.capturarRostroEmp = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    console.log(nombre);
    const arregloRostros = req.body.imagenes;

    //$ Posibles parametros que deberia integrar a la hora de capturar rostros
    // const apellido_Paterno = req.body.apellidoPaterno;
    // const apellido_Materno = req.body.apellidoMaterno;


    // console.log(arregloRostros);
    const ruta =
      "/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/EMPLEADOS/";

    const nombreCarpeta = nombre;

    const rutaCompleta = path.join(ruta, nombreCarpeta);

    fs.mkdir(rutaCompleta, { recursive: true }, (err) => {
      if (err) {
        console.error("Error al crear la carpeta:", err);
      } else {
        console.log("Carpeta creada exitosamente.");
      }
    });

    arregloRostros.forEach((rostro, i) => {
      const base64Data = rostro.replace(/^data:image\/\w+;base64,/, "");
      const img = Buffer.from(base64Data, "base64");
      // fs.writeFileSync(
      //   `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/UMECA-SI-SERVER/IMPUTADOS/${nombreCarpeta}/${i}.jpg`,
      //   img
      // );
      fs.writeFileSync(
        `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/EMPLEADOS/${nombreCarpeta}/${i}.jpg`,
        img
      );
    });

    res.status(200).send("se capturaron las imagenes");
  } catch (error) {
    res.status(500).send("Hubo un error");
  }
};

controlador.eliminarCapturasEmp = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    const rutaEliminar = `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/EMPLEADOS/${nombre}`;
    
    fs.rm(rutaEliminar, { recursive: true, force: true }, (err) => {
      if (err) {
        throw err;
      }
      res.status(200).send(rutaEliminar, "se elimino!");
    });
  } catch (error) {
    res.status(500).send("Hubo un error");
  }
};

export default controlador;
