import sql from "mssql";
import { sqlConfig } from "../conexion.js";

const controlador = {};

controlador.comprobarConexion = async (req, res) => {
  await sql
    .connect(sqlConfig)
    .then(() => {
      res.status(200).send("Se logro la conexion");
    })
    .catch((err) => {
        console.log(err);
      res.status(500).send("No se logro la conexion");
    });

  await sql.close();
};

export default controlador;
