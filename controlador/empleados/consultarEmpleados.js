import { sqlConfig } from "../../conexion.js";
import sql from "mssql"
const controlador = {};

controlador.consultarEmpleados = async (req, res) => {
  try {
    let query = `select * from empleados`;
    let pool = await sql.connect(sqlConfig);

    let result = await pool.request().query(query);

    res.status(200).send(result.recordset);

    await sql.close();
  } catch (error) {
    res.status(500).send("Hubo un error en consultar", error);
  }
};

export {controlador as ce};
