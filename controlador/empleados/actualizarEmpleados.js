import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
import fs from "fs";
import path from "path";
const controlador = {};

controlador.actualizarEmpleado = async (req, res) => {
  let pool;
  try {
    const id_empleado = req.body.id_empleado;
    const nombre = req.body.nombre;
    const apellido_Paterno = req.body.apellidoPaterno;
    const apellido_Materno = req.body.apellidoMaterno;
    const curp = req.body.curp;
    const edad = req.body.edad;
    const telefono = req.body.telefono;
    const rfc = req.body.rfc;
    const cargo = req.body.cargo;
    const curpActual = req.body.curpActual;

    const ruta =
      "/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/EMPLEADOS/";

    const rutaVieja = path.join(ruta, curpActual);

    console.log("Ruta vieja: ", rutaVieja);

    const rutaNueva = path.join(ruta, curp);

    console.log("Ruta nueva: ", rutaNueva);

    const query = `
      exec SPU_actualizarEmpleado @id_empleado=@id_empleado, @nombre=@nombre, @apellido_Paterno=@apellido_Paterno, @apellido_Materno=@apellido_Materno, @curp=@curp, @edad=@edad, @telefono=@telefono, @rfc=@rfc, @cargo=@cargo`;

    pool = await sql.connect(sqlConfig);

    let result = await pool
      .request()
      .input("id_empleado", sql.Int, id_empleado)
      .input("nombre", sql.VarChar(50), nombre)
      .input("apellido_Paterno", sql.VarChar(20), apellido_Paterno)
      .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
      .input("curp", sql.VarChar(18), curp)
      .input("edad", sql.VarChar(100), edad)
      .input("telefono", sql.VarChar(10), telefono)
      .input("rfc", sql.VarChar(13), rfc)
      .input("cargo", sql.VarChar(15), cargo)
      .query(query);

    if (rutaVieja !== rutaNueva) {
      fs.rename(rutaVieja, rutaNueva, (err) => {
        if (err) {
          console.log("Hubo un error al renombrar la carpeta");
          console.error(err);
        } else {
          console.log("Carpeta del imputado renombrada");
        }
      });
    }

    res.status(200).send(result.rowsAffected);
    console.log("Se actualizo");
  } catch (error) {
    res.status(500).send("Hubo un error en consultar", error);
  } finally {
    if (pool) {
      try {
        await pool.close();
      } catch (error) {
        console.log("Hubo un error a la hora de cerrar la conexion");
      }
    }
  }
};

export { controlador as ae };
