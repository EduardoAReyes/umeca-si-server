import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
const controlador = {};

controlador.agregarEmpleado = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    const apellido_Paterno = req.body.apellidoPaterno;
    const apellido_Materno = req.body.apellidoMaterno;
    const curp = req.body.curp;
    const edad = req.body.edad;
    const telefono = req.body.telefono;
    const rfc = req.body.rfc;
    const cargo = req.body.cargo;
    const contrasenia = req.body.password;

    console.log(nombre);
    console.log(apellido_Paterno);
    console.log(apellido_Materno);
    console.log(curp);
    console.log(edad);
    console.log(telefono);
    console.log(rfc);
    console.log(cargo);
    console.log(contrasenia);
    // console.log(fechaFormateada);
    // console.log(horaFormateada);
    let fecha = new Date();

    // Obtener componentes de la fecha
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1; // Se suma 1 porque los meses van de 0 a 11
    let año = fecha.getFullYear();

    // Obtener componentes de la hora
    let hora = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    // Formatear la fecha y hora según el formato requerido
    let fechaFormateada = `${dia}-${mes}-${año}`;
    let horaFormateada = `${hora}:${minutos}:${segundos}`;

    // console.log(apellido_Paterno);
    // console.log(apellido_Materno);
    // console.log('Mi RFC: ',rfc);

    let query = `
    exec SPI_agregarEmpleado @nombre=@nombre, @apellido_Paterno=@apellido_Paterno, @apellido_Materno=@apellido_Materno, @curp=@curp,@edad=@edad, @telefono=@telefono, @rfc=@rfc, @cargo=@cargo, @fechaRegistro=@fechaRegistro, @horaRegistro=@horaRegistro,@contrasenia=@contrasenia`;

    let pool = await sql.connect(sqlConfig);

    let result = await pool
      .request()
      .input("nombre", sql.VarChar(50), nombre)
      .input("apellido_Paterno", sql.VarChar(50), apellido_Paterno)
      .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
      .input("curp", sql.VarChar(18), curp)
      .input("edad", sql.VarChar(100), edad)
      .input("telefono", sql.VarChar(10), telefono)
      .input("rfc", sql.VarChar(13), rfc)
      .input("cargo", sql.VarChar(15), cargo)
      .input("fechaRegistro", sql.VarChar(12), fechaFormateada)
      .input("horaRegistro", sql.VarChar(10), horaFormateada)
      .input("contrasenia", sql.VarChar(100), contrasenia)
      .query(query);

    res.status(200).send(result.rowsAffected);
  } catch (error) {
    res.status(500).send("Hubo un error en agregar empleado:");
  } finally {
    await sql.close();
  }
};

export { controlador as re };
