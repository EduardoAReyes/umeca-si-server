import sql from "mssql";
import { sqlConfig } from "../../conexion.js";
const controlador = {};

controlador.consultarEmpleadosReg = async (req, res) => {
  try {
    let query = "select * from empleados";

    let pool = await sql.connect(sqlConfig);
    let result = await pool.request().query(query);

    let objetos = result.recordset;

    let labels = objetos.map((label) => {
      // return String(label.id_empleado);
      return label.curp;
    });

    res.status(200).send(labels);
  } catch (error) {
    res.status(500).send("Error ocurrido: ", error);
  }
};

export { controlador as cer };
