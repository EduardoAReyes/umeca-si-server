import { sqlConfig } from "../../conexion.js";
import fs from "fs";
import sql from "mssql";
const controlador = {};

controlador.eliminarEmpleado = async (req, res) => {
  try {
    const id_empleado = req.body.id_empleado;
    const nombre = req.body.curp;

    console.log(id_empleado);
    console.log(nombre);

    const rutaEliminar = `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/EMPLEADOS/${nombre}`;

    const query = `delete from empleados where id_empleado=${id_empleado}`;

    let pool = await sql.connect(sqlConfig);

    let result = await pool.request().query(query);

    fs.rm(rutaEliminar, { recursive: true, force: true }, (err) => {
      if (err) {
        throw err;
      }
    });

    res.status(200).send(result.rowsAffected);
  } catch (error) {
    res.status(500).send("Hubo un error en eliminar");
  } finally {
    await sql.close();
  }
};

export { controlador as ee };
