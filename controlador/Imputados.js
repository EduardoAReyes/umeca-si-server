import sql from "mssql";
import { sqlConfig } from "../conexion.js";
import fs from "fs";
import path from "path";
const controlador = {};

/**
 * En este codigo se crea la funcion para poder ingresar un nuevo imputado a nuestra base de datos.
 */

/**
 * Nombre
 * Apellidos
 * Edad
 * Domicilio
 * Telefono
 * Delito
 * Status (Cumpliendo/Imcumpliendo)
 * 
 * 
 * CREATE TABLE imputados(
	id_imputado int identity(1,1) primary key,
	nombre varchar(50),
	apellido_Paterno varchar(50),
	apellido_Materno varchar(50),
	edad varchar(100),
	domicilio varchar(80),
	telefono varchar(10),
	delito varchar(80),
	estado varchar(12),
); 
 */

// controlador.consultarAsistenciaImputados = async (req, res) => {
//   try {
//     const query = "select * from asistenciaImputados ORDER BY id_asistencia DESC";

//     let pool = await sql.connect(sqlConfig);
//     let result = await pool.request().query(query);

//     res.status(200).send(result.recordset);

//     await sql.close();
//   } catch (error) {
//     res.status(500).send("Hubo un error: ", error);
//   }
// };

// controlador.registrarAsistenciaImputado = async (req, res) => {
//   try {
//     const nombre = req.body.nombreImp;
//     // const arregloRostros = req.body.imagenes;
//     // const apellidoPaterno = req.body.apellidoPaterno;
//     // const apellidoMaterno = req.body.apellidoMaterno;

//     console.log("Nombre para asistencia: ", nombre);
//     const apellidoPaterno = "";
//     const apellidoMaterno = "";

//     let fecha = new Date();

//     // Obtener componentes de la fecha
//     let dia = fecha.getDate();
//     let mes = fecha.getMonth() + 1; // Se suma 1 porque los meses van de 0 a 11
//     let año = fecha.getFullYear();

//     // Obtener componentes de la hora
//     let hora = fecha.getHours();
//     let minutos = fecha.getMinutes();
//     let segundos = fecha.getSeconds();

//     // Formatear la fecha y hora según el formato requerido
//     let fechaFormateada = `${dia}-${mes}-${año}`;
//     let horaFormateada = `${hora}:${minutos}:${segundos}`;

//     const query = `exec SPI_InsertarAsistenciaImputado @nombre=@nombre, @apellidoPaterno=@apellidoPaterno, @apellidoMaterno=@apellidoMaterno, @fecha=@fecha, @hora=@hora`;

//     let pool = await sql.connect(sqlConfig);

//     let result = await pool
//       .request()
//       .input("nombre", sql.VarChar(50), nombre)
//       .input("apellidoPaterno", sql.VarChar(50), apellidoPaterno)
//       .input("apellidoMaterno", sql.VarChar(50), apellidoMaterno)
//       .input("fecha", sql.VarChar(12), fechaFormateada)
//       .input("hora", sql.VarChar(10), horaFormateada)
//       .query(query);

//     res.status(200).send(result.rowsAffected);
//     await sql.close();
//   } catch (error) {
//     res.status(500).send("hubo un error en registrar asistencia", error);
//   }
// };

// controlador.consultarImputadosReg = async (req, res) => {
//   try {
//     let query = "select * from imputados";

//     let pool = await sql.connect(sqlConfig);
//     let result = await pool.request().query(query);

//     let objetos = result.recordset;
//     console.log("Objetos: ", objetos);
//     // console.log('result consultar imp: ',result.recordset);

//     let labels = objetos.map((label) => {
//       return label.nombre;
//     });

//     console.log(labels);

//     res.status(200).send(labels);
//   } catch (error) {
//     console.log("Hubo un error en consulta", error);
//   }
// };

controlador.capturarRostroImpu = async (req, res) => {
  try {
    const nombre = req.body.curp;
    const arregloRostros = req.body.imagenes;
    // const apellidoPaterno = req.body.apellidoPaterno;
    // const apellidoMaterno = req.body.apellidoMaterno;

    // let fecha = new Date();

    // // Obtener componentes de la fecha
    // let dia = fecha.getDate();
    // let mes = fecha.getMonth() + 1; // Se suma 1 porque los meses van de 0 a 11
    // let año = fecha.getFullYear();

    // // Obtener componentes de la hora
    // let hora = fecha.getHours();
    // let minutos = fecha.getMinutes();
    // let segundos = fecha.getSeconds();

    // // Formatear la fecha y hora según el formato requerido
    // let fechaFormateada = `${dia}-${mes}-${año}`;
    // let horaFormateada = `${hora}:${minutos}:${segundos}`;

    // const query = `exec SPI_InsertarAsistenciaImputado @nombre=@nombre, @apellidoPaterno=@apellidoPaterno, @apellidoMaterno=@apellidoMaterno, @fecha=@fecha, @hora=@hora`;

    // const ruta ="/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/UMECA-SI-SERVER/IMPUTADOS";

    // const ruta = "/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/IMPUTADOS/"

    const ruta =
      "/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/IMPUTADOS/";

    const nombreCarpeta = nombre;

    const rutaCompleta = path.join(ruta, nombreCarpeta);

    fs.mkdir(rutaCompleta, { recursive: true }, (err) => {
      if (err) {
        console.error("Error al crear la carpeta:", err);
      } else {
        console.log("Carpeta creada exitosamente.");
      }
    });

    arregloRostros.forEach((rostro, i) => {
      const base64Data = rostro.replace(/^data:image\/\w+;base64,/, "");
      const img = Buffer.from(base64Data, "base64");
      // fs.writeFileSync(
      //   `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/UMECA-SI-SERVER/IMPUTADOS/${nombreCarpeta}/${i}.jpg`,
      //   img
      // );
      fs.writeFileSync(
        `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/IMPUTADOS/${nombreCarpeta}/${i}.jpg`,
        img
      );
    });

    res.status(200).send("se capturaron las imagenes");
  } catch (error) {
    res.status(500).send("Hubo un error", error);
  }
};

// controlador.consultarImputado = async (req, res) => {
//   try {
//     const query = "select * from imputados";

//     let pool = await sql.connect(sqlConfig);
//     let resultado = await pool.request().query(query);

//     res.status(200).send(resultado.recordset);

//     await sql.close();
//   } catch (error) {
//     res.status(500).send("Error en consultar imputado: ", error);
//   }
// };

// controlador.ingresarImputado = async (req, res) => {
//   try {
//     const nombre = req.body.nombre;
//     const apellido_Paterno = req.body.apellido_Paterno;
//     const apellido_Materno = req.body.apellido_Materno;
//     const edad = req.body.edad;
//     const domicilio = req.body.domicilio;
//     const telefono = req.body.telefono;
//     const delito = req.body.delito;
//     const estado = req.body.estado;
//     const CURP = req.body.CURP;
//     const fechaRegistro = req.body.fechaRegistro;
//     const horaRegistro = req.body.horaRegistro;

//     // console.log(nombre);
//     // const nombre = "Lalo"
//     // const apellido_Paterno="Reyes"
//     // const apellido_Materno = "Rosales"
//     // const edad = "15"
//     // const domicilio = "Cuautla"
//     // const telefono="7351905877"
//     // const delito = "robo"
//     // const estado = "si"

//     const query = `exec SPI_ingresarImputado @nombre=@nombre, @apellido_Paterno=@apellido_Paterno, @apellido_Materno=@apellido_Materno, @edad=@edad, @domicilio=@domicilio, @telefono=@telefono, @delito=@delito, @estado=@estado`;

//     let pool = await sql.connect(sqlConfig);
//     let resultado = await pool
//       .request()
//       .input("nombre", sql.VarChar(50), nombre)
//       .input("apellido_Paterno", sql.VarChar(50), apellido_Paterno)
//       .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
//       .input("edad", sql.VarChar(100), edad)
//       .input("domicilio", sql.VarChar(100), domicilio)
//       .input("telefono", sql.VarChar(100), telefono)
//       .input("delito", sql.VarChar(80), delito)
//       .input("estado", sql.VarChar(12), estado)
//       .input("CURP", sql.VarChar(18), CURP)
//       .input("fechaRegistro", sql.Date, fechaRegistro)
//       .input("horaRegistro", sql.Time, horaRegistro)
//       .query(query);

//     res.status(200).send(resultado.rowsAffected);
//     await sql.close();
//   } catch (error) {
//     res.status(500).send("Hubo un error", error);
//   }
// };

// controlador.actualizarImputado = async (req, res) => {
//   try {
//     const id_imputado = req.body.id_imputado;
//     const nombre = req.body.nombre;
//     const apellido_Paterno = req.body.apellido_Paterno;
//     const apellido_Materno = req.body.apellido_Materno;
//     const edad = req.body.edad;
//     const domicilio = req.body.domicilio;
//     const telefono = req.body.telefono;
//     const delito = req.body.delito;
//     const estado = req.body.estado;

//     const query = `
//     exec SPU_actualizarImputado @id_imputado= @id_imputado, @nombre=@nombre, @apellido_Paterno=@apellido_Paterno, @apellido_Materno=@apellido_Materno, @edad=@edad, @domicilio=@domicilio, @telefono=@telefono, @delito=@telefono, @estado=@estado`;

//     let pool = await sql.connect(sqlConfig);

//     let resultado = await pool
//       .request()
//       .input("id_imputado", sql.Int, id_imputado)
//       .input("nombre", sql.VarChar(50), nombre)
//       .input("apellido_Paterno", sql.VarChar(50), apellido_Paterno)
//       .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
//       .input("edad", sql.VarChar(100), edad)
//       .input("domicilio", sql.VarChar(100), domicilio)
//       .input("telefono", sql.VarChar(100), telefono)
//       .input("delito", sql.VarChar(80), delito)
//       .input("estado", sql.VarChar(12), estado)
//       .query(query);

//     res.send(resultado.rowsAffected);
//     console.log("Se actualizo");
//     await sql.close();
//   } catch (error) {
//     res.status(500).send("Hubo un error: ", error);
//   }
// };

// controlador.eliminarImputado = async (req, res) => {
//   try {
//     const id_imputado = req.body.id_imputado;
//     const query = `delete from imputados where id_imputado=${id_imputado}`;

//     let pool = await sql.connect(sqlConfig);
//     let resultado = await pool.request().query(query);

//     res.send(resultado.rowsAffected);

//     console.log("Se elimino");
//     await sql.close();
//   } catch (error) {
//     res.status(500).send("Error en eliminar imputado: ", error);
//   }
// };

export default controlador;
