import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
import fs from "fs";
const controlador = {};

controlador.eliminarImputado = async (req, res) => {
  try {
    const id_imputado = req.body.id_imputado;
    const curp_imputado = req.body.curp;
    const rutaEliminar = `/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/IMPUTADOS/${curp_imputado}`;
    const query = `delete from imputados where id_imputado=${id_imputado}`;

    let pool = await sql.connect(sqlConfig);
    let resultado = await pool.request().query(query);

    fs.rm(rutaEliminar, { recursive: true, force: true }, (err) => {
      if (err) {
        throw err;
      }
    });

    res.status(200).send(resultado.rowsAffected);

    console.log("Se elimino");
  } catch (error) {
    res.status(500).send("Error en eliminar imputado: ", error);
  } finally {
    await sql.close();
  }
};

export { controlador as ei };
