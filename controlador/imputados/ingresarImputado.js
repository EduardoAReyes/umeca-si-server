import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
const controlador = {};

controlador.ingresarImputado = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    const apellido_Paterno = req.body.apellido_Paterno;
    const apellido_Materno = req.body.apellido_Materno;
    const edad = req.body.edad;
    const genero = req.body.genero;
    const domicilio = req.body.domicilio;
    const telefono = req.body.telefono;
    const delito = req.body.delito;
    const estado = req.body.estado;
    const curp = req.body.curp;

    console.log("Genero: ", genero);

    //! const fechaRegistro = req.body.fechaRegistro;
    //! const horaRegistro = req.body.horaRegistro;

    // console.log(nombre);
    // const nombre = "Lalo"
    // const apellido_Paterno="Reyes"
    // const apellido_Materno = "Rosales"
    // const edad = "15"
    // const domicilio = "Cuautla"
    // const telefono="7351905877"
    // const delito = "robo"
    // const estado = "si"

    let fecha = new Date();

    // Obtener componentes de la fecha
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1; // Se suma 1 porque los meses van de 0 a 11
    let año = fecha.getFullYear();

    // Obtener componentes de la hora
    let hora = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    // Formatear la fecha y hora según el formato requerido
    let fechaRegistro = `${dia}-${mes}-${año}`;
    let horaRegistro = `${hora}:${minutos}:${segundos}`;

    // console.log(genero);
    // console.log(curp);

    const query = `exec SPI_ingresarImputado 
    @nombre=@nombre, 
    @apellido_Paterno=@apellido_Paterno, 
    @apellido_Materno=@apellido_Materno, 
    @edad=@edad,
    @domicilio=@domicilio, 
    @telefono=@telefono, 
    @delito=@delito, 
    @estado=@estado, 
    @curp=@curp, 
    @genero=@genero

    `;

    let pool = await sql.connect(sqlConfig);
    let resultado = await pool
      .request()
      .input("nombre", sql.VarChar(50), nombre)
      .input("apellido_Paterno", sql.VarChar(50), apellido_Paterno)
      .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
      .input("edad", sql.VarChar(100), edad)
      .input("domicilio", sql.VarChar(100), domicilio)
      .input("telefono", sql.VarChar(100), telefono)
      .input("delito", sql.VarChar(80), delito)
      .input("estado", sql.VarChar(12), estado)
      .input("curp", sql.VarChar(18), curp)
      .input("genero", sql.VarChar(9), genero)
      // .input("fechaRegistro", sql.Date, fechaRegistro)
      // .input("horaRegistro", sql.Time, horaRegistro)
      .query(query);

    res.status(200).send(resultado.rowsAffected);
    await sql.close();
  } catch (error) {
    res.status(500).send("Hubo un error");
  }
};

export { controlador as ii };
