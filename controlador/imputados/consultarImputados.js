import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
const controlador = {};

controlador.consultarImputado = async (req, res) => {
  try {
    const query = "select * from imputados";

    let pool = await sql.connect(sqlConfig);
    let resultado = await pool.request().query(query);

    res.status(200).send(resultado.recordset);

    await sql.close();
  } catch (error) {
    res.status(500).send("Error en consultar imputado: ", error);
  }
};

export { controlador as ci };
