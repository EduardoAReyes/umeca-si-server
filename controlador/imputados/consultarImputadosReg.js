import sql from 'mssql'
import { sqlConfig } from "../../conexion.js";
const controlador = {};

controlador.consultarImputadosReg = async (req, res) => {
  try {
    let query = "select * from imputados";

    let pool = await sql.connect(sqlConfig);
    let result = await pool.request().query(query);

    let objetos = result.recordset;
    console.log("Objetos: ", objetos);
    // console.log('result consultar imp: ',result.recordset);

    let labels = objetos.map((label) => {
      return label.curp;
    });

    console.log(labels);

    res.status(200).send(labels);
  } catch (error) {
    console.log("Hubo un error en consulta", error);
  }
};

export { controlador as cir };
