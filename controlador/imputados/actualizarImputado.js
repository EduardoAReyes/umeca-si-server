import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
import fs from "fs";
import path from "path";
const controlador = {};

controlador.actualizarImputado = async (req, res) => {
  let pool;
  try {
    const id_imputado = req.body.id_imputado;
    const nombre = req.body.nombre;
    const apellido_Paterno = req.body.apellido_Paterno;
    const apellido_Materno = req.body.apellido_Materno;
    const edad = req.body.edad;
    const domicilio = req.body.domicilio;
    const telefono = req.body.telefono;
    const delito = req.body.delito;
    const estado = req.body.estado;
    const curp = req.body.curp;
    const curpActual = req.body.curpActual;
    const genero = req.body.genero;

    const ruta =
      "/Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/tauri-trial-faceapi/public/IMPUTADOS/";

    const rutaVieja = path.join(ruta, curpActual);

    console.log("Ruta vieja: ", rutaVieja);

    const rutaNueva = path.join(ruta, curp);

    console.log("Ruta nueva: ", rutaNueva);
    const query = `
      exec SPU_actualizarImputado @id_imputado= @id_imputado, @nombre=@nombre, @apellido_Paterno=@apellido_Paterno, @apellido_Materno=@apellido_Materno, @edad=@edad, @domicilio=@domicilio, @telefono=@telefono, @delito=@delito, @estado=@estado, @curp=@curp, @genero=@genero`;

    pool = await sql.connect(sqlConfig);
    let resultado = await pool
      .request()
      .input("id_imputado", sql.Int, id_imputado)
      .input("nombre", sql.VarChar(50), nombre)
      .input("apellido_Paterno", sql.VarChar(50), apellido_Paterno)
      .input("apellido_Materno", sql.VarChar(50), apellido_Materno)
      .input("edad", sql.VarChar(100), edad)
      .input("domicilio", sql.VarChar(100), domicilio)
      .input("telefono", sql.VarChar(100), telefono)
      .input("delito", sql.VarChar(80), delito)
      .input("estado", sql.VarChar(12), estado)
      .input("curp", sql.VarChar(18), curp)
      .input("genero", sql.VarChar(9), genero)
      .query(query);

    if (rutaVieja !== rutaNueva) {
      fs.rename(rutaVieja, rutaNueva, (err) => {
        if (err) {
          console.log("Hubo un error al renombrar la carpeta");
          console.error(err);
        } else {
          console.log("Carpeta del imputado renombrada");
        }
      });
    }

    res.status(200).send(resultado.rowsAffected);
    console.log("Se actualizo");
  } catch (error) {
    res.status(500).send("Hubo un error en actualizar imputado");
  } finally {
    if (pool) {
      try {
        await pool.close();
      } catch (error) {
        console.log("Hubo un error a la hora de cerrar la conexion");
      }
    }
  }
};

export { controlador as ai };
