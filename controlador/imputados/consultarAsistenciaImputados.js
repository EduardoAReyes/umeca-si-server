import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
const controlador = {};

controlador.consultarAsistenciaImputados = async (req, res) => {
  let pool;
  try {
    const query =
      "select * from asistenciaImputados ORDER BY id_asistencia DESC";

    pool = await sql.connect(sqlConfig);
    let result = await pool.request().query(query);

    res.status(200).send(result.recordset);
  } catch (error) {
    res.status(500).send("Hubo un error: ", error);
  } finally {
    if (pool) {
      try {
        await pool.close();
      } catch (error) {
        console.err("Hubo un error en consulta asistencia");
      }
    }
  }
};

export { controlador as cai };
