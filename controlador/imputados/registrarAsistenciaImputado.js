import { sqlConfig } from "../../conexion.js";
import sql from "mssql";
const controlador = {};

controlador.registrarAsistenciaImputado = async (req, res) => {
  let pool;
  try {
    const nombre = req.body.nombreImp;

    let fecha = new Date();

    // Obtener componentes de la fecha
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1; // Se suma 1 porque los meses van de 0 a 11
    let año = fecha.getFullYear();

    // Obtener componentes de la hora
    let hora = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    // Formatear la fecha y hora según el formato requerido
    let fechaFormateada = `${dia}-${mes}-${año}`;
    let horaFormateada = `${hora}:${minutos}:${segundos}`;

    const query = `exec SPI_InsertarAsistenciaImputado @nombre=@nombre, @fecha=@fecha, @hora=@hora`;

    pool = await sql.connect(sqlConfig);

    let result = await pool
      .request()
      .input("nombre", sql.VarChar(50), nombre)
      .input("fecha", sql.VarChar(12), fechaFormateada)
      .input("hora", sql.VarChar(10), horaFormateada)
      .query(query);

    res.status(200).send(result.rowsAffected);
  } catch (error) {
    res.status(500).send("hubo un error en registrar asistencia", error);
  } finally {
    if (pool) {
      try {
        await pool.close();
      } catch (error) {
        console.error(error);
      }
    }
  }
};

export { controlador as rai };
