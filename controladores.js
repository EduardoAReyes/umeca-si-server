import { ce } from "./controlador/empleados/consultarEmpleados.js";
import { ae } from "./controlador/empleados/actualizarEmpleados.js";
import { ee } from "./controlador/empleados/eliminarEmpleado.js";
import { re } from "./controlador/empleados/agregarEmpleados.js";
import { cer } from "./controlador/empleados/consultarEmpleadosReg.js";
import { ci } from "./controlador/imputados/consultarImputados.js";
import { ii } from "./controlador/imputados/ingresarImputado.js";
import { ei } from "./controlador/imputados/eliminarImputado.js";
import { ai } from "./controlador/imputados/actualizarImputado.js";
import { cir } from "./controlador/imputados/consultarImputadosReg.js";
import { cai } from "./controlador/imputados/consultarAsistenciaImputados.js";
import { rai } from "./controlador/imputados/registrarAsistenciaImputado.js";

const barril = {
  consultarEmpleado: ce.consultarEmpleados,
  agregarEmpleado: re.agregarEmpleado,
  eliminarEmpleado: ee.eliminarEmpleado,
  actualizarEmpleado: ae.actualizarEmpleado,
  consultarEmpleadosReg: cer.consultarEmpleadosReg,
  consultarImputado: ci.consultarImputado,
  ingresarImputado: ii.ingresarImputado,
  eliminarImputado: ei.eliminarImputado,
  actualizarImputado: ai.actualizarImputado,
  consultarImputadosReg: cir.consultarImputadosReg,
  consultarAsistenciaImputados: cai.consultarAsistenciaImputados,
  registrarAsistenciaImputado: rai.registrarAsistenciaImputado,
};

export default barril;
